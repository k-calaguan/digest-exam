$(document).ready(function() {
	let bgBody = 0;

	$('#darkmode-btn').on({click: function() {

		if(bgBody == 0) {
			$(this).css('color', '#0088d6');

			$('body').css('background', 'linear-gradient(rgba(0, 0, 0, 0.85), rgba(0, 0, 0, 0.5)), url(../img/hammer-bg.png)');
			$('body').css('background-size', 'cover');
			$('body').css('background-attachment', 'fixed');
			$('#trans-box1, #trans-box2, #trans-box3').css('background-color', '#000');
			$('#trans-box1, #trans-box2, #trans-box3').css('color', '#fff');
			$('#trans-box1, #trans-box2, #trans-box3').css('opacity', '0.85');
			bgBody++;
			console.log('dark');
			console.log(bgBody);
		} else {
			$(this).css('color', '#fff');

			$('body').css('background', 'linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0)), url(../img/hammer-bg.png)');
			$('body').css('background-size', 'cover');
			$('body').css('background-attachment', 'fixed');
			$('#trans-box1, #trans-box2, #trans-box3').css('background-color', '#fff');
			$('#trans-box1, #trans-box2, #trans-box3').css('color', '#000');
			$('#trans-box1, #trans-box2, #trans-box3').css('opacity', '0.85');
			bgBody--;
			console.log('light');
			console.log(bgBody);
		}
	}});
});