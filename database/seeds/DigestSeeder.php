<?php

use Illuminate\Database\Seeder;
use App\Digest;

class DigestSeeder extends Seeder
{
	/**
	* Run the database seeds.
	*
	* @return void
	*/
	public function run()
	{
		$faker = Faker\Factory::create();
		$arrayValues = ['civil', 'criminal', 'taxation','commercial','labor','political','remedial','ethics'];
		$rateValues = ['0', '1', '2','3','4','5'];

		for($i = 0; $i < 100; $i++) {
			Digest::create([
			'author' => $faker->name, 
			'title' => $faker->text(50),
			'category' => $arrayValues[rand(0,7)],
			'summary' => $faker->text(500),
			'doctrine' => $faker->text(500),
			'facts' => $faker->text(500),
			'issues_ratio' => $faker->text(500),
			'dispositive' => $faker->text(500),
			'note' => $faker->text(200),	            
			'rating' => $rateValues[rand(0,5)],
			]);
		}
	}
}
