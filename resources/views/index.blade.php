<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<title>Digest Exam</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{ asset('./css/style.css') }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	
</head>
<body>
	<div class="container mt-5 mb-5">
		<div id="top" class="row">
			<div class="col-sm-12 col-md-7 col-lg-8">
				<div id="trans-box1" >
					<h5>
						<b>Digest of:</b>
						<h6>{{$digest->title}}</h6>
					</h5>
				</div>
			</div>

			<div class="col-sm-12 col-md-5 col-lg-4">
				<div id="trans-box2">
					<h6><b>Author:<span> {{$digest->author}}</span></b></h6>
					<h6><b>Subject:<span> {{ucfirst($digest->category)}}</span></b></h6>
					<h6><b>Rating:<span>
						@if($digest->rating == 0)
							No rating
						@else
							@for($i = 0; $i < $digest->rating; $i++)
								<i class="fa fa-star"></i>
							@endfor
						@endif
					</span></b></h6>
				</div>
			</div>

			<div class="col-sm-12 col-md-12 col-lg-12">
				<div id="trans-box3">
					<div>
						<h5><b>Summary:</b>
						<h6>{{$digest->summary}}</h6>
					</div>

					<div>
						<h5><b>Doctrine:</b></h5>
						<h6>{{$digest->summary}}</h6>
					</div>

					<div>
						<h5><b>Facts:</b></h5>
						<h6>{{$digest->facts}}</h6>
					</div>

					<div>
						<h5><b>Issues_ratio:</b></h5>
						<h6>{{$digest->issues_ratio}}</h6>
					</div>

					<div>
						<h5><b>Dispositive:</b></h5>
						<h6>{{$digest->dispositive}}</h6>
					</div>

					<div>
						<h5><b>Other Notes:</b></h5>
						<h6>{{$digest->note}}</h6>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div id="btn-float">
		<i id="darkmode-btn" class="fa fa-lightbulb-o"></i>
		<a href="#top"><i id="arrow-btn" class="fa fa-arrow-circle-up"></i></a>
	</div>

	<script type="text/javascript" src="{{ asset('./js/script.js') }}"></script>
</body>
</html>
