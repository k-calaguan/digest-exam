<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Digest;

class DigestController extends Controller
{
    function index(){
    	$digest = Digest::first();
    	return view('index')->with('digest',$digest);
    }
}
